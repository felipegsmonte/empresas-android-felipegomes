package com.example.ioasysempresas.main.domain

import retrofit2.Call
import com.example.ioasysempresas.model.Enterprises
import com.example.ioasysempresas.model.Headers
import com.example.ioasysempresas.service.AppEmpresasAPI
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Callback
import retrofit2.Response

class MainInteractorImpl: MainInteractor, AppEmpresasAPI() {
    override fun getEmpresas(busca: String, headers: Headers, listener: GetEmpresasListener) {
        GlobalScope.launch{
            lateinit var enterprises: Enterprises
            val call: Call<Enterprises> = retrofitService.getEmpresas(busca, headers.accessToken, headers.client, headers.uid)
            call.enqueue(object : Callback<Enterprises> {
                override fun onFailure(call: Call<Enterprises>, t: Throwable) {
                    GlobalScope.launch(Dispatchers.Main) { listener.onFailure() }
                }

                override fun onResponse(call: Call<Enterprises>, response: Response<Enterprises>) {
                    if(response.isSuccessful){
                        if(response.code() == 200){
                            enterprises = response.body()!!
                            GlobalScope.launch(Dispatchers.Main) { listener.onSuccess(enterprises) }
                        }else{
                            GlobalScope.launch(Dispatchers.Main) { listener.onCredentialsFailure() }
                        }
                    }else{
                        GlobalScope.launch(Dispatchers.Main) { listener.onFailure() }
                    }
                }

            })
        }
    }

}