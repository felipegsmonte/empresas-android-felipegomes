package com.example.ioasysempresas.main.ui

import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ioasysempresas.R
import com.example.ioasysempresas.login.ui.LoginActivity
import com.example.ioasysempresas.main.domain.MainInteractorImpl
import com.example.ioasysempresas.main.presentation.MainPresenter
import com.example.ioasysempresas.main.presentation.MainPresenterImpl
import com.example.ioasysempresas.main.presentation.MainView
import com.example.ioasysempresas.model.Enterprise
import com.example.ioasysempresas.model.Enterprises
import com.example.ioasysempresas.model.HeadersSingleton
import com.example.ioasysempresas.model.UserReturnSingleton
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast


class MainActivity : AppCompatActivity(), MainView {

    private lateinit var mPresenter: MainPresenter
    private lateinit var mAdapter: MainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mPresenter = MainPresenterImpl(MainInteractorImpl())
        mPresenter.attach(this)

        val text = searchView.context.resources.getIdentifier("android:id/search_src_text", null, null)
        val plate = searchView.context.resources.getIdentifier("android:id/search_plate", null, null)
        val button = searchView.context.resources.getIdentifier("android:id/search_button", null, null)
        val clear = searchView.context.resources.getIdentifier("android:id/search_close_btn", null, null)
        val searchIcon = searchView.context.resources.getIdentifier("android:id/search_mag_icon", null, null)

        val textView = searchView.findViewById(text) as TextView
        val plateView = searchView.findViewById(plate) as View
        val buttonView = searchView.findViewById(button) as ImageView
        val clearView = searchView.findViewById(clear) as ImageView
        val searchIconView = searchView.findViewById(searchIcon) as ImageView

        plateView.background.setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY)
        textView.setTextColor(Color.WHITE)
        buttonView.setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY)
        clearView.setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY)
        searchIconView.setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY)

        searchView.setOnSearchClickListener {
            imgToolbar.visibility = View.GONE
            mainText.visibility = View.GONE
        }
        searchView.setOnCloseListener {
            imgToolbar.visibility = View.VISIBLE
            false
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                showProgress()
                (mPresenter as MainPresenterImpl).getEmpresas(query!!, HeadersSingleton.instance)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean { // do something when text changes
                return false
            }
        })

        arrowBack.setOnClickListener {
            closeEmpresa()
        }
    }

    override fun doLogout() {
        UserReturnSingleton.clearInfo()
        HeadersSingleton.clearInfo()
        startActivity<LoginActivity>()
        finishAffinity()
    }

    override fun bindInfo(enterprises: Enterprises) {

        if(enterprises.enterprises!!. isEmpty()){
            hideProgress()
            searchRecyclerView.visibility = View.GONE
            emptyText.visibility = View.VISIBLE
        }

        mAdapter = MainAdapter(enterprises.enterprises!!)

        searchRecyclerView.layoutManager = LinearLayoutManager(this)
        searchRecyclerView.adapter = mAdapter
        mAdapter.notifyDataSetChanged()

        hideProgress()
    }

    override fun showMessage(message: String) {
        hideProgress()
        toast(message)
    }

    override fun openEmpresa(enterprise: Enterprise) {
        Picasso.get().load(enterprise.photo).into(mainImg)
        mainDescricao.text = enterprise.description
        topName.text = enterprise.enterpriseName

        searchView.visibility = View.GONE
        imgToolbar.visibility = View.GONE
        topName.visibility = View.VISIBLE
        arrowBack.visibility = View.VISIBLE
        searchRecyclerView.visibility = View.GONE
        mainText.visibility = View.GONE
        searchProgressBar.visibility = View.GONE
        emptyText.visibility = View.GONE
        mainCV.visibility = View.VISIBLE
    }

    override fun closeEmpresa() {
        searchView.visibility = View.VISIBLE
        topName.visibility = View.GONE
        arrowBack.visibility = View.GONE
        searchRecyclerView.visibility = View.VISIBLE
        mainText.visibility = View.GONE
        searchProgressBar.visibility = View.GONE
        emptyText.visibility = View.GONE
        mainCV.visibility = View.GONE
    }

    override fun showProgress() {
        searchRecyclerView.visibility = View.GONE
        mainText.visibility = View.GONE
        searchProgressBar.visibility = View.VISIBLE
        emptyText.visibility = View.GONE

    }

    override fun hideProgress() {
        searchRecyclerView.visibility = View.VISIBLE
        mainText.visibility = View.GONE
        searchProgressBar.visibility = View.GONE
    }
}
