package com.example.ioasysempresas.login.presentation

import com.example.ioasysempresas.login.domain.DoSignInListener
import com.example.ioasysempresas.login.domain.LoginInteractor
import com.example.ioasysempresas.model.Headers
import com.example.ioasysempresas.model.User
import com.example.ioasysempresas.model.UserReturn

class LoginPresenterImpl(private val loginInteractor: LoginInteractor): LoginPresenter {

    private var mView: LoginView? = null

    override fun doSignIn(user: User) {
        loginInteractor.doSignIn(user, doSignInListener)
    }

    private val doSignInListener = object :DoSignInListener{
        override fun onSuccess(userReturn: UserReturn, headers: Headers) {
            mView!!.doLogin(userReturn, headers)
            mView!!.hideProgress()
        }

        override fun onFailure() {
            mView!!.hideProgress()
            mView!!.showMessage()
        }

        override fun onCredentialsFailure() {
            mView!!.showError()
        }

    }

    override fun attach(view: LoginView) {
        mView = view
    }

    override fun detach() {
        mView = null
    }
}