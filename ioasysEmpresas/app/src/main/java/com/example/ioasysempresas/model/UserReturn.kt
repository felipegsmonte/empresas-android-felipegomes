package com.example.ioasysempresas.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class UserReturn {
    @SerializedName("investor")
    @Expose
    var investor: Investor_? = null
    @SerializedName("enterprise")
    @Expose
    var enterprise: Any? = null
    @SerializedName("success")
    @Expose
    var success: Boolean? = null

}

object UserReturnSingleton{
    var instance = UserReturn()

    fun clearInfo(){
        instance.investor = null
        instance.enterprise = null
        instance.success = false
    }
}