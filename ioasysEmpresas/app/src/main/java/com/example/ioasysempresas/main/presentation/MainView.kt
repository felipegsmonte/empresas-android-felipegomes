package com.example.ioasysempresas.main.presentation

import com.example.ioasysempresas.core.LoadingView
import com.example.ioasysempresas.model.Enterprise
import com.example.ioasysempresas.model.Enterprises

interface MainView: LoadingView {

    fun doLogout()

    fun bindInfo(enterprises: Enterprises)

    fun showMessage(message: String)

    fun openEmpresa(enterprise: Enterprise)

    fun closeEmpresa()
}