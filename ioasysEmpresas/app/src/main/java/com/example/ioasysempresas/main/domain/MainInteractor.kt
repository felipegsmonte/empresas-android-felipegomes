package com.example.ioasysempresas.main.domain

import com.example.ioasysempresas.model.Enterprises
import com.example.ioasysempresas.model.Headers

interface MainInteractor {

    fun getEmpresas(busca: String, headers: Headers, listener: GetEmpresasListener)
}

interface GetEmpresasListener{

    fun onSuccess(enterprises: Enterprises)

    fun onFailure()

    fun onCredentialsFailure()
}