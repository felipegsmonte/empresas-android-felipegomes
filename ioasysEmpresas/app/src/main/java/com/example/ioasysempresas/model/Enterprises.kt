package com.example.ioasysempresas.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Enterprises {
    @SerializedName("enterprises")
    @Expose
    var enterprises: List<Enterprise>? = null

}