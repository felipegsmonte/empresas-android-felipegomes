package com.example.ioasysempresas.main.presentation

import com.example.ioasysempresas.main.domain.GetEmpresasListener
import com.example.ioasysempresas.main.domain.MainInteractor
import com.example.ioasysempresas.model.Enterprises
import com.example.ioasysempresas.model.Headers

class MainPresenterImpl (private val mainInteractor: MainInteractor): MainPresenter {

    private var mView: MainView? = null

    override fun getEmpresas(busca: String, headers: Headers) {
        mainInteractor.getEmpresas(busca, headers, getEmpresasListener)
    }

    private val getEmpresasListener = object :GetEmpresasListener{
        override fun onSuccess(enterprises: Enterprises) {
            mView!!.bindInfo(enterprises)
            mView!!.hideProgress()
        }

        override fun onFailure() {
            mView!!.hideProgress()
            mView!!.showMessage("Ocorreu um erro! Tente novamente!")
        }

        override fun onCredentialsFailure() {
            mView!!.showMessage("Sua sessão expirou!")
            mView!!.doLogout()
        }

    }

    override fun attach(view: MainView) {
        mView = view
    }

    override fun detach() {
        mView = null
    }
}