package com.example.ioasysempresas.core

interface BasePresenter<in V> {

    fun attach(view: V)

    fun detach()
}