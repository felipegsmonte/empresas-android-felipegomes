package com.example.ioasysempresas.login.domain

import com.example.ioasysempresas.model.Headers
import com.example.ioasysempresas.model.User
import com.example.ioasysempresas.model.UserReturn

interface LoginInteractor {
    fun doSignIn(user: User, listener: DoSignInListener)
}

interface DoSignInListener{

    fun onSuccess(userReturn: UserReturn, headers: Headers)

    fun onFailure()

    fun onCredentialsFailure()
}