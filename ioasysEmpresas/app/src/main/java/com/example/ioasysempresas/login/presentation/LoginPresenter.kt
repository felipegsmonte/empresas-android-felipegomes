package com.example.ioasysempresas.login.presentation

import com.example.ioasysempresas.core.BasePresenter
import com.example.ioasysempresas.model.User

interface LoginPresenter: BasePresenter<LoginView> {

    fun doSignIn(user: User)
}