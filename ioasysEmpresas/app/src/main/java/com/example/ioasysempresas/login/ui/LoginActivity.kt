package com.example.ioasysempresas.login.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.ioasysempresas.R
import com.example.ioasysempresas.login.domain.LoginInteractorImpl
import com.example.ioasysempresas.login.presentation.LoginPresenter
import com.example.ioasysempresas.login.presentation.LoginPresenterImpl
import com.example.ioasysempresas.login.presentation.LoginView
import com.example.ioasysempresas.main.ui.MainActivity
import com.example.ioasysempresas.model.*
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast


class LoginActivity : AppCompatActivity(), LoginView {

    private lateinit var mPresenter: LoginPresenter
    private var user = User()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mPresenter = LoginPresenterImpl(LoginInteractorImpl())
        mPresenter.attach(this)
    }

    override fun onStart() {
        super.onStart()

        cvEntrar.setOnClickListener {
            showProgress()
            user.email = editEmail.text.toString()
            user.password = editPassword.text.toString()

            mPresenter.doSignIn(user)
        }

        editEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                hideError()
            }
        })

        editPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                hideError()
            }
        })
    }

    override fun doLogin(userReturn: UserReturn, headers: Headers) {
        UserReturnSingleton.instance = userReturn
        HeadersSingleton.instance = headers
        startActivity<MainActivity>()
        finishAffinity()
    }

    override fun showProgress() {
        progressBarHolder.visibility = View.VISIBLE
        txtErro.visibility = View.GONE
        cvEntrar.isEnabled = false
        editEmail2.isEnabled = false
        editPassword2.isEnabled = false
    }

    override fun hideProgress() {
        progressBarHolder.visibility = View.GONE
        cvEntrar.isEnabled = true
        editEmail2.isEnabled = true
        editPassword2.isEnabled = true
    }

    override fun showError() {
        progressBarHolder.visibility = View.GONE
        txtErro.visibility = View.VISIBLE
        cvEntrar.isEnabled = true
        editEmail2.isEnabled = true
        editPassword2.isEnabled = true
    }

    override fun showMessage() {
        toast("Ocorreu um erro ao tentar logar! Tente novamente!")
    }

    override fun hideError() {
        txtErro.visibility = View.GONE
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detach()
    }
}
