package com.example.ioasysempresas.login.domain

import com.example.ioasysempresas.model.*
import com.example.ioasysempresas.service.AppEmpresasAPI
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class LoginInteractorImpl : LoginInteractor, AppEmpresasAPI(){
    override fun doSignIn(user: User, listener: DoSignInListener) {
        GlobalScope.launch{
            val headers = Headers()
            lateinit var userReturn: UserReturn
            val call: Call<UserReturn> = retrofitService.doSignIn(user)
            call.enqueue(object : Callback<UserReturn>{
                override fun onFailure(call: Call<UserReturn>, t: Throwable) {
                    GlobalScope.launch(Dispatchers.Main) { listener.onFailure() }
                }

                override fun onResponse(call: Call<UserReturn>, response: Response<UserReturn>) {
                    if(response.isSuccessful){
                        headers.accessToken = response.headers().get("access-token").toString()
                        headers.client = response.headers().get("client").toString()
                        headers.uid = response.headers().get("uid").toString()

                        userReturn = response.body()!!

                        GlobalScope.launch(Dispatchers.Main) { listener.onSuccess(userReturn, headers) }
                    }else{
                        GlobalScope.launch(Dispatchers.Main) { listener.onCredentialsFailure() }
                    }
                }

            })
        }


    }

}