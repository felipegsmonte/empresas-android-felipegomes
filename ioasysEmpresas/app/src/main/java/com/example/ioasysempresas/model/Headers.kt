package com.example.ioasysempresas.model

class Headers(var accessToken: String = "",
              var client: String = "",
              var uid: String = "")

object HeadersSingleton{
    var instance = Headers()

    fun clearInfo(){
        instance.accessToken = ""
        instance.client = ""
        instance.uid = ""
    }
}