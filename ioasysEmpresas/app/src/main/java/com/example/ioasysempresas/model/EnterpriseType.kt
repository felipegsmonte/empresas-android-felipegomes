package com.example.ioasysempresas.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class EnterpriseType {
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("enterprise_type_name")
    @Expose
    var enterpriseTypeName: String? = null

}