package com.example.ioasysempresas.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Enterprise {
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("email_enterprise")
    @Expose
    var emailEnterprise: Any? = null
    @SerializedName("facebook")
    @Expose
    var facebook: Any? = null
    @SerializedName("twitter")
    @Expose
    var twitter: Any? = null
    @SerializedName("linkedin")
    @Expose
    var linkedin: Any? = null
    @SerializedName("phone")
    @Expose
    var phone: Any? = null
    @SerializedName("own_enterprise")
    @Expose
    var ownEnterprise: Boolean? = null
    @SerializedName("enterprise_name")
    @Expose
    var enterpriseName: String? = null
    @SerializedName("photo")
    @Expose
    var photo: String? = null
    @SerializedName("description")
    @Expose
    var description: String? = null
    @SerializedName("city")
    @Expose
    var city: String? = null
    @SerializedName("country")
    @Expose
    var country: String? = null
    @SerializedName("value")
    @Expose
    var value: Int? = null
    @SerializedName("share_price")
    @Expose
    var sharePrice: Double? = null
    @SerializedName("enterprise_type")
    @Expose
    var enterpriseType: EnterpriseType? = null

}