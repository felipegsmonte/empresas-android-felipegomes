package com.example.ioasysempresas.main.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ioasysempresas.R
import com.example.ioasysempresas.model.Enterprise
import com.squareup.picasso.Picasso

class MainAdapter(private val enterprisesList: List<Enterprise>) : RecyclerView.Adapter<EnterprisesViewHolder>() {

    private lateinit var mContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EnterprisesViewHolder {
        mContext = parent.context
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_empresa, parent, false)
        return EnterprisesViewHolder(v)
    }

    override fun getItemCount() = enterprisesList.size

    override fun onBindViewHolder(holder: EnterprisesViewHolder, position: Int) {
        val enterprise = enterprisesList[position]

        if(!enterprise.photo.isNullOrBlank()){
            Picasso.get().load(enterprise.photo).into(holder.itemEmpresaImg)
        }
        holder.itemEmpresaNome.text = enterprise.enterpriseName
        holder.itemEmpresaTipo.text = enterprise.enterpriseType!!.enterpriseTypeName
        holder.itemEmpresaPais.text = enterprise.country

        holder.itemView.setOnClickListener {
            (mContext as MainActivity).openEmpresa(enterprise)
        }
    }
}

class EnterprisesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val itemEmpresaImg: ImageView = itemView.findViewById(R.id.itemEmpresaImg)
    val itemEmpresaNome: TextView = itemView.findViewById(R.id.itemEmpresaNome)
    val itemEmpresaTipo: TextView = itemView.findViewById(R.id.itemEmpresaTipo)
    val itemEmpresaPais: TextView = itemView.findViewById(R.id.itemEmpresaPais)
}