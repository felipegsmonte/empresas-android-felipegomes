package com.example.ioasysempresas.login.presentation

import com.example.ioasysempresas.core.LoadingView
import com.example.ioasysempresas.model.Headers
import com.example.ioasysempresas.model.UserReturn

interface LoginView: LoadingView {

    fun doLogin(userReturn: UserReturn, headers: Headers)

    fun showError()

    fun hideError()

    fun showMessage()

}