package com.example.ioasysempresas.core

interface LoadingView {

    fun showProgress()

    fun hideProgress()

}