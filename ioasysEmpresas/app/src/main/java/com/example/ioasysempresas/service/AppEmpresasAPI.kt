package com.example.ioasysempresas.service

import com.example.ioasysempresas.BuildConfig
import com.example.ioasysempresas.model.Enterprises
import com.example.ioasysempresas.model.User
import com.example.ioasysempresas.model.UserReturn
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

open class AppEmpresasAPI {
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()

    protected val retrofitService: AppEmpresasEndpoints = retrofit.create(AppEmpresasEndpoints::class.java)
}

interface AppEmpresasEndpoints{
    @POST("users/auth/sign_in")
    fun doSignIn(@Body user: User): Call<UserReturn>

    @GET("enterprises")
    fun getEmpresas(@Query("name") nome: String,
                    @Header("access-token") accessToken: String,
                    @Header("client") client: String,
                    @Header("uid") uid: String): Call<Enterprises>

}