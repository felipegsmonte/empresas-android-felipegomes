package com.example.ioasysempresas.main.presentation

import com.example.ioasysempresas.core.BasePresenter
import com.example.ioasysempresas.model.Headers

interface MainPresenter: BasePresenter<MainView> {

    fun getEmpresas(busca: String, headers: Headers)
}