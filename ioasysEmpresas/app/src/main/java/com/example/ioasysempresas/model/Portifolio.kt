package com.example.ioasysempresas.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Portfolio {
    @SerializedName("enterprises_number")
    @Expose
    var enterprisesNumber: Int? = null
    @SerializedName("enterprises")
    @Expose
    var enterprises: List<Any>? = null

}