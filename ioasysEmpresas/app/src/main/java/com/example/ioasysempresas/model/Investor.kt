package com.example.ioasysempresas.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Investor_ {
    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("investor_name")
    @Expose
    var investorName: String? = null
    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("city")
    @Expose
    var city: String? = null
    @SerializedName("country")
    @Expose
    var country: String? = null
    @SerializedName("balance")
    @Expose
    var balance: Double? = null
    @SerializedName("photo")
    @Expose
    var photo: String? = null
    @SerializedName("portfolio")
    @Expose
    var portfolio: Portfolio? = null
    @SerializedName("portfolio_value")
    @Expose
    var portfolioValue: Double? = null
    @SerializedName("first_access")
    @Expose
    var firstAccess: Boolean? = null
    @SerializedName("super_angel")
    @Expose
    var superAngel: Boolean? = null

}