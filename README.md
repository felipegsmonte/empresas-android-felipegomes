![N|Solid](logo_ioasys.png)

# README #

* Integração disponível a partir de uma collection para Postman (https://www.getpostman.com/apps) disponível neste repositório.
* O `README.md` deve conter uma pequena justificativa de cada biblioteca adicionada ao projeto como dependência.
* O `README.md` deve conter tambem o que você faria se tivesse mais tempo.
* O `README.md` do projeto deve conter instruções de como executar a aplicação
* Independente de onde conseguiu chegar no teste é importante disponibilizar seu fonte para analisarmos.

#Bibliotecas#

* Retrofit para fazer as requisições da API.
* Kotlin Coroutine para as coroutines na hora das requisições.
* Picasso para abrir as imagens no aplicativo.
* Anko para minimizar a quantidade de código. Ex.: startActivity, toast, etc.

#Caso tivesse mais tempo#

* Organizaria melhor o código usando mais funções e classes ao invés de deixar tantas coisas nas Activities principais.
* Provavelmente usaria Fragments para melhorar a navegação entre as telas de busca e dos dados da empresa.
* Diferenciaria mais as exceções do login (credenciais erradas, validação de email...).

#Instruções de execução#

* Autenticação normal com o email e senha fornecidos.
* Foi usado um SearchView na ToolBar que se abre clicando na lupa à esquerda.
* A busca é feita confirmando a busca no teclado.
* Com a lista aberta basta selecionar a empresa dentre as listadas para que se abram os detalhes.
